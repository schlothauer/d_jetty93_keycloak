Based on official jetty alpine image with additional keycloak adapter files

The image contains:
    - Java8 JRE
    - Jetty 9.4
    - Keycloak client 3.2.1.final

[More information here](https://keycloak.gitbooks.io/securing-client-applications-guide/content/topics/oidc/java/jetty9-adapter.html)


